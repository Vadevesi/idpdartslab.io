// Init canvas and some other important global variables.
let canvas = document.getElementById('canvas');
let WIDTH = Math.min(window.innerWidth * 0.7, window.innerHeight * 0.95);
let HEIGHT = WIDTH;
canvas.width = window.innerWidth * 0.7;
canvas.height = window.innerWidth * 0.7;
let RADIUS = (WIDTH - (WIDTH * 0.12))/2
let STARTNB = 301;
var scoreBefore = STARTNB;
var scoreAfter = STARTNB;

let ID = {9: 6, 1: 8, 7: 4, 6: 18, 5: 1, 4: 20, 3: 5, 2: 12, 1: 9, 0: 14, 19: 11, 18: 8, 17: 16, 16: 7, 15: 19, 14: 3, 13: 17, 12: 2, 11: 15, 10: 10, 8: 13}
let DI = {10: 0, 15: 1, 2: 2, 17: 3, 3: 4, 19: 5, 7: 6, 16: 7, 8: 8, 11: 9, 14: 10, 9: 11, 12: 12, 5: 13, 20: 14, 1: 15, 18: 16, 4: 17, 13: 18, 6: 19}
let highlight = {1: [], 2: [], 3: [], 25: false, 50: false}
let grey = {1: [], 2: [], 3: [], 25: false, 50: false}
let scoreTable = document.getElementById('scoreTable');
let totalScoreTable = document.getElementById('totalScoreTable');
totalScoreTable.rows[1].cells[0].innerText = scoreBefore;
let solPane = document.getElementById('sol');
let solTable = document.getElementById('solTable');
let playerThrows = {}

function drawBoard() {
  ctx = canvas.getContext('2d');  
  ctx.clearRect(0, 0, WIDTH, HEIGHT);
  
  let arcLength = Math.PI/10 
  let offset = Math.PI/200
  red = false

  console.log(grey)
  ctx.font = '20px Arial';
  for (let i = 0; i < 20; i++) {

    // Outer ring
    ctx.beginPath();
    if (highlight[2].includes(i)) {
      ctx.fillStyle = 'orange';
    } else if (grey[1].includes(i)) {
      ctx.fillStyle = 'grey';
    } else if (red) {
      ctx.fillStyle = 'green';
    } else {
      ctx.fillStyle = 'red';
    }
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.arc(WIDTH/2, HEIGHT/2, RADIUS, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.fill();

    // Inside outer ring
    ctx.beginPath();
    if (highlight[1].includes(i)) {
      ctx.fillStyle = 'orange';
      console.log(i);
    } else if (grey[1].includes(i)) {
      console.log(i);
      ctx.fillStyle = '#ebebeb';
    } else if (red) {
      ctx.fillStyle = 'white';
    } else {
      ctx.fillStyle = 'black';
    }
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.95, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.fill();

    // Outer inside ring
    ctx.beginPath();
    if (highlight[3].includes(i)) {
      ctx.fillStyle = 'orange';
    } else if (grey[1].includes(i)) {
      ctx.fillStyle = 'grey';
    } else if (red) {
      ctx.fillStyle = 'green';
    } else {
      ctx.fillStyle = 'red';
    }
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.6, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.fill();

    // Inside inner ring
    ctx.beginPath();
    if (highlight[1].includes(i)) {
      ctx.fillStyle = 'orange';
    } else if (grey[1].includes(i)) {
      ctx.fillStyle = '#ebebeb';
    } else if (red) {
      ctx.fillStyle = 'white';
    } else {
      ctx.fillStyle = 'black';
    }
    red = !red;

    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.55, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
    ctx.lineTo(WIDTH/2, HEIGHT/2);
    ctx.fill();

    ctx.fillStyle = 'black';
    ctx.textAlign = 'center';
    let begin = arcLength * i + offset
    let textX = 1.06 * RADIUS * Math.cos(begin) + WIDTH/2;
    let textY = 1.06 * RADIUS * Math.sin(begin) + HEIGHT/2;
    let idx = ID[(i + 9)%20]
    // let idx = i;

    ctx.fillText(idx, textX, textY);
    // console.log(i, idx);

  }
  // Outer bullseye
  ctx.beginPath();
  if (highlight[25]) {
    ctx.fillStyle = 'orange';
  } else {
    ctx.fillStyle = 'green';
  }
  ctx.lineTo(WIDTH/2, HEIGHT/2);
  ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.10, 0, 2*Math.PI);
  ctx.lineTo(WIDTH/2, HEIGHT/2);
  ctx.fill();

  // Inner bullseye
  ctx.beginPath();
  if (highlight[50]) {
    ctx.fillStyle = 'orange';
  } else {
    ctx.fillStyle = 'red';
  }
  ctx.lineTo(WIDTH/2, HEIGHT/2);
  ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.05, 0, 2*Math.PI);
  ctx.lineTo(WIDTH/2, HEIGHT/2);
  ctx.fill();
}

function highlightPlane(plane) {
  if (plane['val'] === 0) {
    return;
  }
  let arcLength = Math.PI/10 
  let offset = Math.PI/200

  let i = plane['id'] - 10;
  let mult = plane['mult'];
  let val = plane['val'];


  if (i < 0) {
    i = 20 + i;
  }
  // console.log(i, plane['id'])
  if (val === 25 | val === 50) {
    highlight[val] = true;
  } else {
    highlight[mult].push(i);
  }

  //ctx.beginPath();
  //ctx.fillStyle = 'orange';
  //ctx.lineTo(WIDTH/2, HEIGHT/2);
  //ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.95, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
  //ctx.lineTo(WIDTH/2, HEIGHT/2);
  //ctx.fill();
}

function coordToRadial(x, y) {
  x -= WIDTH/2
  y -= HEIGHT/2
  x -= 0.0142 * WIDTH;  // We need to do this offset, for some reason.
  y -= 0.0142 * HEIGHT;
  let rad = Math.atan2(y, x);
  let r = (x*x+y*y)**(1/2)

  let arcLength = Math.PI/10 
  let offset = Math.PI/200
  let id = Math.floor((rad-offset-arcLength/2)/(arcLength)) + 10;

  let mult = 0;
  let val = 0;
  let text = '';
  if (RADIUS * 0.95 <= r && r <= RADIUS * 1.00) {
    mult = 2;
    val = ID[id];
    text = 'double ' + val;
  } else if (RADIUS * 0.55 <= r && r <= RADIUS * 0.65) {
    mult = 3;
    val = ID[id];
    text = 'triple ' + val;
  } else if (RADIUS * 0.05 <= r && r <= RADIUS * 0.13) {
    mult = 1;
    val = 25;
    text = val;
  } else if (r <= RADIUS * 0.05) {
    mult = 1;
    val = 50
    text = 'bullseye!';
  } else if (r <= RADIUS) {
    mult = 1;
    val = ID[id];
    text = 'single ' + val;
  } else {
    mult = 1;
    val = 0;
    text = 'miss';
  }
  return {'mult': mult, 'val': val,
          'id': id, 'text': text};
}

function updateScoreboard() {
  let totalThrown = 0;
  for (let i = 0; i < 3; i++) {
    if (playerThrows.hasOwnProperty(i)) {
      scoreTable.rows[i+1].cells[0].innerText = playerThrows[i]['text'];
    }
  }
  // totalScore.innerText = Number(totalScore.innerText - score);

  totalScoreTable.rows[1].cells[0].innerText = scoreBefore;
  totalScoreTable.rows[1].cells[1].innerText = scoreAfter;
}

function resetScoreboard() {

  scoreTable.rows[1].cells[0].innerText = '\xa0';
  scoreTable.rows[2].cells[0].innerText = '\xa0'; 
  scoreTable.rows[3].cells[0].innerText = '\xa0';
}

function updateScore(plane) {
  // Set text
  let scoreText = ''
  let score = 0
  scoreText += plane['val'];

  let throwId = Object.keys(playerThrows).length;
  if (throwId < 3) {
    // Add new throw.
    playerThrows[throwId] = plane;
    
    // Subtract throw from current toal.
    if ( 1 <= plane['val'] && plane['val'] <= 20) {
      scoreAfter -= plane['mult'] * plane['val'];
    } else {
      scoreAfter -= plane['val'];
    }

  } else {
    // After 3 throws, reset the throwlist.
    playerThrows = {};
    highlight = {1: [], 2: [], 3: [], 25: false, 50: false}
    scoreBefore = scoreAfter;

    resetScoreboard();
  }
  updateScoreboard();
  drawBoard();

  // setVal('Finish', true);
	modelExpand();

  // propagate();

}

function showSols(sols) {
  for (let i = 1; i < 4; i++) {
    for (let solId = 0; solId < 3; solId++) {
      if (!sols.hasOwnProperty(solId)) {
        // In case less than 3 sols were found.
        solTable.rows[i].cells[solId].innerText = '/';
        continue
      }
      let val = sols[solId]['Throw' + i];
      let mult = sols[solId]['Mult' + i];
      if (mult == 3) {
        mult = 'T ';
      } else if (mult == 2) {
        mult = 'D ';
      } else if (mult == 1) {
        mult = 'S ';
      } else {
        mult = '';
      }
      solTable.rows[i].cells[solId].innerText = mult + val;
    }
  }
}

/* If no solution possible, display this. */
function noSols() {
  for (let i = 1; i < 4; i++) {
    for (let j = 0; j < 3; j++) {
      solTable.rows[i].cells[j].innerText = '/';
    }
  }
  //solTable.rows[1].cells[1].innerText = 'No finish';
}

function clickHandler(e) {
  // Find which plane was clicked, color it & update total score.
  plane = coordToRadial(e.clientX, e.clientY);
  highlightPlane(plane)
  updateScore(plane);
}

canvas.addEventListener('click', clickHandler);
drawBoard();


function greyOut(negdict) {
  if (!negdict) {
    return;
  }

  let throwId = Object.keys(playerThrows).length + 1;
  impossibleVal = negdict['Throw' + throwId];
  console.log(impossibleVal);
  if (impossibleVal) {
    for (let i = 0; i < impossibleVal.length; i++) {
      let idx = DI[impossibleVal[i]];
      grey[1].push(idx);
    }
  }
  drawBoard();
}

// IDP related stuff.
function initKb() {
  theory = `vocabulary V {
      type nb := {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 50}
      type mult := {1, 2, 3}
      Throw1: () -> nb
      Mult1: () -> mult
      Throw2: () -> nb
      Mult2: () -> mult
      Throw3: () -> nb
      Mult3: () -> mult
  
      Begin: () -> Int
      Score: () -> Int
      End: () -> Int
  
      Finish: () -> Bool
  }
  
  theory T:V {
  
      End() = Begin() - Score().
  
      [End score needs to be zero to finish]
      (Finish() <=> End() = 0).
  
      Score() = Throw1() * Mult1() + Throw2() * Mult2() + Throw3() * Mult3().
  
      Finish() & Throw3() ~= 0 => Mult3() = 2.
      Finish() & Throw3() = 0 & Throw2() ~= 0 => Mult2() = 2.
      Finish() & Throw3() = 0 & Throw2() = 0 & Throw1() ~= 0 => Mult1() = 2.

      Throw1() = 25 | Throw1() = 50 => Mult1() = 1.
      Throw2() = 25 | Throw2() = 50 => Mult2() = 1.
      Throw3() = 25 | Throw3() = 50 => Mult3() = 1.
  }
  
  structure S:V {
  }
  
  procedure main() {
      pretty_print(model_expand(T,S,0))
  }
  `
  socket.send(JSON.stringify({'method': 'create',
                              'theory': theory}));
}

function initSocket() {
  //socket = new WebSocket("ws://localhost:8765");
  socket = new WebSocket("wss://websocket.simonvandevelde.be");
  // if (navigator.userAgent.indexOf("Chrome") != -1) {
  //   // Use WSS for Chrome.
  //   socket = new WebSocket("wss://idpsocket.herokuapp.com/");
  // } else {
  //   // Use WS else FF complains about HerokuApp's cert not being valid.
  //   // Should be fixed once we have proper hosting.
  //   socket = new WebSocket("ws://idpsocket.herokuapp.com/");
  // }

  socket.onopen = function(e) {
    initKb();
  }
  socket.onmessage = function(event) {
    data = JSON.parse(event.data);
    if (data['method'] === 'modelexpand') {
      if (data['success']) {
			  console.log(data);
        // solPane.innerText = 'Finish possible!';
        showSols(data['models']);
      } else {
        // solPane.innerText = 'Cannot finish';
        noSols();
			  console.log(data);
      }
    } else if (data['method'] === 'propagate') {
      greyOut(data['negdict'])
      console.log(data);
    } else if (data['method'] === 'explain') {
    } else {
      console.log(data);
    }
  }
}


function setVal(symbol, val) {
  msg = {'method': 'setval',
         'symbol': symbol,
         'val': val};
  socket.send(JSON.stringify(msg));
}

function prepareIDP() {
  msg = {'method': 'setval',
         'symbol': 'Finish',
         'val': true};
  socket.send(JSON.stringify(msg));

  msg = {'method': 'setval',
         'symbol': 'Begin',
         'val': scoreBefore};
  socket.send(JSON.stringify(msg));

  // Update the already made throws.
  for (let i = 0; i < 3; i++) {
    if (playerThrows.hasOwnProperty(i)) {
      symb = 'Throw' + (i + 1);
      msg = {'method': 'setval',
             'symbol': symb,
             'val': playerThrows[i]['val']};
      socket.send(JSON.stringify(msg));
      symb = 'Mult' + (i + 1);
      msg = {'method': 'setval',
             'symbol': symb,
             'val': playerThrows[i]['mult']};
      socket.send(JSON.stringify(msg));
    } else {
      symb = 'Throw' + (i + 1);
      msg = {'method': 'setval',
             'symbol': symb,
             'val': null};
      symb = 'Mult' + (i + 1);
      socket.send(JSON.stringify(msg));
      msg = {'method': 'setval',
             'symbol': symb,
             'val': null};
      socket.send(JSON.stringify(msg));
    }
  }
}

function modelExpand() {
  prepareIDP();
  msg = {'method': 'modelexpand',
         'number': 3};
  socket.send(JSON.stringify(msg));
}

function propagate() {
  prepareIDP();
  let throwId = Object.keys(playerThrows).length;
  if (throwId === 3) {
    throwId = 0;
  }
  msg = {'method': 'propagate',
         'symbol': 'Throw' + (throwId + 1)}
  socket.send(JSON.stringify(msg));
}

// function reset() {
//   geojson.eachLayer(function(layer) {
//     layer.color = 'none';
//     layer.disallowedColors = [];
//     geojson.resetStyle(layer);
//   })
// }
  

initSocket()

